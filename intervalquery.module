<?php


/**
 * @file intervalquery module
 * The "intervalquery" module provides an API for requesting
 * info created or modified in a given interval:
 * between start/end dates. It is licensed under GPLv2.
 *
 * @author Larry Hamel, Codeguild.com
*/

/**
* Implementation of hook_help.
* Provide a few help tips
*/
function intervalquery_help($section) {
	switch ($section) {
		case 'admin/modules#name' :
			$ret = 'Intervalquery XML-RPC module';
			break;
		case 'admin/modules#description' :
			$ret = 'Module for answering an XML-RPC query for information created or updated in a given interval';
			break;
		case 'admin/help#rpc' :
			$ret = 'Help for the Intervalquery XML-RPC module';
			break;
	}
	return $ret;
}

/**
* Implementation of hook_settings.
* Define a drupal setting for the allowed IP address
*/
function intervalquery_settings() {
	$form['rpc_allowed_ip'] = array (
		'#type' => 'textfield',
		'#title' => t('Allowed IP address in canonical format'),
		'#default_value' => variable_get('rpc_allowed_ip', '127.0.0.1'),
		'#size' => 15,
		'#maxlength' => 15,
		'#description' => t('RPC calls to this module from any other IP will be rejected. ')
						. t('This does NOT affect other XML-RPC modules.'));

	return $form;
}

function intervalquery_nodes($params) {
	$from = _intervalquery_get_from($params);
	$until = _intervalquery_get_until($params);

	return array (
		'nodes' => _interval_query_nodes($from, $until)
	);
}

function intervalquery_comments($params) {
	$from = _intervalquery_get_from($params);
	$until = _intervalquery_get_until($params);

	return array (
		'comments' => _interval_query_comments($from, $until)
	);
}

function intervalquery_users($params) {
	$from = _intervalquery_get_from($params);
	$until = _intervalquery_get_until($params);

	return array (
		'users' => _interval_query_users($from, $until)
	);
}

function intervalquery_all($params) {
	$from = _intervalquery_get_from($params);
	$until = _intervalquery_get_until($params);

	return array (
		'users' => _interval_query_users($from, $until),
		'nodes' => _interval_query_nodes($from, $until),
		'comments' => _interval_query_comments($from, $until),
	);
}

function _intervalquery_get_from($params) {
	$from = 0;

	// did we get from param?
	$fromparam = $params['from'];
	if ( $fromparam  ) {
		// convert from iso to unix time
		$from = intervalquery_iso2timestamp($fromparam);
	}

	return $from;
}

function _intervalquery_get_until($params) {
	$until = 0;

	// did we get until param?
	$untilparam = $params['until'];
	if ( $untilparam  ) {
		// convert from iso to unix time
		$until = intervalquery_iso2timestamp($untilparam);
	}

	return $until;
}


function _interval_query_nodes($from, $until) {
	$package = array();

	$package['from'] = intervalquery_timestamp2iso($from);
	$package['until'] = intervalquery_timestamp2iso($until);

	$count = db_result(db_query("SELECT COUNT(n.nid) FROM {node} n WHERE n.changed >= %d AND n.changed <= %d", $from, $until));
	$package['count'] = "$count";

	$nodes = array();
	if ($rows = db_query("SELECT * FROM {node} n WHERE n.changed >= %d AND n.changed <= %d", $from, $until)) {
		while ($anode = db_fetch_object($rows)) {
			// must load to get latest body
			// TODO get latest body from join in query above !!!!!!!!!!!!
			$mynode = node_load($anode->nid);

			$nid = $anode->nid;
			$body = $mynode->body; // get latest version;

			// must convert to array so that we can add on body to node metadata
			$nodeasarray = (array) $anode;
			$nodeasarray['body'] = $body;

			// convert dates
			$nodeasarray['created'] = intervalquery_timestamp2iso($nodeasarray['created']);
			$nodeasarray['changed'] = intervalquery_timestamp2iso($nodeasarray['changed']);

			$nodes[$nid] = $nodeasarray;
		}
	}

	$package['nodes'] = $nodes;
	return $package;
}

function _interval_query_comments($from, $until) {
	$package = array();

	$package['from'] = intervalquery_timestamp2iso($from);
	$package['until'] = intervalquery_timestamp2iso($until);

	$count = db_result(db_query("SELECT COUNT(c.cid) FROM {comments} c WHERE c.timestamp >= %d AND c.timestamp <= %d", $from, $until));
	$package['count'] = "$count";

	$comments = array();
	if ($rows = db_query("SELECT * FROM {comments} c WHERE c.timestamp >= %d AND c.timestamp <= %d", $from, $until)) {
		while ($item = db_fetch_object($rows)) {
			$asarray = (array) $item;
			$asarray['timestamp'] = intervalquery_timestamp2iso($asarray['timestamp']);
			$comments[$item->cid] = $asarray;
		}
	}

	$package['comments'] = $comments;
	return $package;
}

function _interval_query_users($from, $until) {
	$package = array();

	$package['from'] = intervalquery_timestamp2iso($from);
	$package['until'] = intervalquery_timestamp2iso($until);

	$count = db_result(db_query("SELECT COUNT(u.uid) FROM {users} u WHERE u.created >= %d AND u.created <= %d", $from, $until));
	$package['count'] = "$count";

	$users = array();
	if ($rows = db_query("SELECT * FROM {users} u WHERE u.created >= %d AND u.created <= %d", $from, $until)) {
		while ($item = db_fetch_object($rows)) {
			$asarray = (array) $item;
			$asarray['created'] = intervalquery_timestamp2iso($asarray['created']);
			$asarray['pass'] = "";
			$users[$item->cid] = $asarray;
		}
	}

	$package['comments'] = $users;
	return $package;
}

/**
* Implementation of hook_xmlrpc
*/
function intervalquery_xmlrpc() {
	// TODO get var setting
	//  if (variable_get('rpc_allowed_ip', '') != $_SERVER['REMOTE_ADDR']) {
	if ('127.0.0.1' == $_SERVER['REMOTE_ADDR']) {

		$ret = array (
			'intervalquery.nodes' => 'intervalquery_nodes',
			'intervalquery.comments' => 'intervalquery_comments',
			'intervalquery.users' => 'intervalquery_users',
			'intervalquery.all' => 'intervalquery_all'
		);

	} else {
		$ret = array (
			'intervalquery.nodes' => 'intervalquery_rpc_ip_ban',
			'intervalquery.comments' => 'intervalquery_rpc_ip_ban',
			'intervalquery.users' => 'intervalquery_rpc_ip_ban',
			'intervalquery.all' => 'intervalquery_rpc_ip_ban'
		);
	}

	return $ret;
}

/**
* Return an error message.
*/
function intervalquery_rpc_ip_ban() {
	return t("This address is not allowed to access this RPC service");
}

/**
 * convert timestamp to iso 8601
 */
function intervalquery_timestamp2iso($timestamp) {
	return date('Y-m-d\TH:i:sO', $timestamp);
}


/**
* convert (extended) ISO 8601 compliant date string to unix timestamp
*
* @param    string $datestr ISO 8601 compliant date string
*/
function intervalquery_iso2timestamp($datestr){
	$eregStr =
	'([0-9]{4})-'.	// centuries & years CCYY-
	'([0-9]{2})-'.	// months MM-
	'([0-9]{2})'.	// days DD
	'T'.			// separator T
	'([0-9]{2}):'.	// hours hh:
	'([0-9]{2}):'.	// minutes mm:
	'([0-9]{2})(\.[0-9]+)?'. // seconds ss.ss...
	'(Z|[+\-][0-9]{2}:?[0-9]{2})?'; // Z to indicate UTC, -/+HH:MM:SS.SS... for local tz's
	if(ereg($eregStr,$datestr,$regs)){
		// not utc
		if($regs[8] != 'Z'){
			$op = substr($regs[8],0,1);
			$h = substr($regs[8],1,2);
			$m = substr($regs[8],strlen($regs[8])-2,2);
			if($op == '-'){
				$regs[4] = $regs[4] + $h;
				$regs[5] = $regs[5] + $m;
			} elseif($op == '+'){
				$regs[4] = $regs[4] - $h;
				$regs[5] = $regs[5] - $m;
			}
		}
		return gmmktime($regs[4], $regs[5], $regs[6], $regs[2], $regs[3], $regs[1]);
	} else {
		return false;
	}
}
